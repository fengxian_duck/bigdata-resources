package sdust.lab207.data;

import weka.classifiers.meta.FilteredClassifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.unsupervised.attribute.Remove;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月14日 下午5:39:06 
 * @description 即时过滤
 */
public class FilteringOnTheFly {
	
	public static void main(String[] args) throws Exception {
		// 加载数据集
		Instances train = DataSource.read("data\\segment-challenge.arff");
		Instances test = DataSource.read("data\\segment-test.arff");
		
		//由于本例需要预测测试集的类别，因此一定要设置类别属性，否则会产生运行时错误。
		//设置分类属性
		train.setClassIndex(train.numAttributes()-1);//
		test.setClassIndex(test.numAttributes()-1);
		
		//检查训练集和测试集合是否兼容
		if(!train.equalHeaders(test)){
			throw new Exception("训练集和测试集不兼容！"+train.equalHeaders(test));
		}
		
		//过滤器 
		Remove rm = new Remove();
		rm.setAttributeIndices("1");//删除第一个属性
		
		
		//分类器
		J48 j48 = new J48();
		j48.setUnpruned(true);//设置未裁剪的J48
		
		//元分类器
		FilteredClassifier fc = new FilteredClassifier();
		fc.setFilter(rm);
		fc.setClassifier(j48);
		
		//训练并预测
		fc.buildClassifier(train);
		for(int i=0;i<test.numAttributes();i++){
			double pred = fc.classifyInstance(test.instance(i));
			System.out.print("编号："+(i+1));
			System.out.print(",实际编号："+test.classAttribute().value((int)test.instance(i).classValue()));
			
			System.out.print(",预测类别："+test.classAttribute().value((int)pred));
			System.out.println();
			
		}
		
		/**
		 * 注意：这里使用 classifyInstance方法，功能对给定的测试实例进行分类。该方法的唯一参数是要进行分类的测试实例对象，方法的返回值是给定实例最优可能的预测类别，
		 * 如果没有做出预测，则返回Utils.missingValue()
		 */
		
		
	}
	
	

}
