package sdust.lab207.serialization;

import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ConverterUtils.DataSource;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月17日 下午9:11:02 
 * @description 模型序列化
 */
public class ModelSerialization {
	
	public static void main(String[] args) throws Exception {
		Instances data = DataSource.read("data\\weather.numeric.arff");
		// 设置类别属性索引
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes() - 1);
		}
		
		J48 cls = new J48();
		cls.buildClassifier(data);
		
		//序列化模型
		SerializationHelper.write("data\\j48.model", cls);
		System.out.println("Serialization OK!");
		
		//反序列化模型
		Classifier cls2 = (Classifier)SerializationHelper.read("data\\j48.model");
		
		System.out.println("Reverse Serialization OK !");
		System.out.println("RS MOdel IS :");
		System.out.println(cls2);
		
	}

}
