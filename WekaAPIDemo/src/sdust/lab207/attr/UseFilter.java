package sdust.lab207.attr;

import java.awt.image.TileObserver;

import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;

/**
 * @author LbZhang
 * @version 创建时间：2016年6月17日 上午10:22:33
 * @description 使用过滤器
 * 
 */
public class UseFilter {
	public static void main(String[] args) throws Exception {
		Instances data = DataSource.read("data\\weather.numeric.arff");

		// 设置类别属性索引
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes() - 1);
		}
		
		System.out.println("使用过滤器");
		AttributeSelection filter = new AttributeSelection();
		CfsSubsetEval cseval = new CfsSubsetEval();
		GreedyStepwise search = new GreedyStepwise();
		search.setSearchBackwards(true);
		filter.setEvaluator(cseval);
		filter.setSearch(search);
		//filter.setInputFormat(data);
		
		
	//	Instances newData = Filter.useFilter(data, (Filter)filter);
		
	}

}
