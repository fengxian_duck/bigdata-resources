package sdust.lab207.attr;

import java.util.Random;

import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.AttributeSelectedClassifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月16日 下午5:16:04 
 * @description 使用元分类器
 * 
 * AttributeSelectedClassifier 使用元分类器
 * AttributeSelectedClassifier与分类器FilterClassifier类似，但不采用将基分类器和过滤器作为参数执行过滤的方法，而是使用一个由
 * weka.attributeSelection.ASEvaluation派生的评估器来，以及有一个weka.attributeSelection.ASSearch派生的搜索算法
 * 执行属性选择，并由基分类器对精简后的数据进行训练。
 * J48作为基分类器  CfsSubsetEval作为评估器  GreedyStepwise作为搜索算法
 * 
 */
public class UseMetaClassifier {
	public static void main(String[] args) throws Exception {
		Instances data = DataSource.read("data\\weather.numeric.arff");
		
		//设置类别属性索引
		if(data.classIndex()==-1){
			data.setClassIndex(data.numAttributes()-1);
			
		}
		
		System.out.println("使用元分类器");
		
		//元分类选择器
		AttributeSelectedClassifier  classifier = new AttributeSelectedClassifier();
		
		CfsSubsetEval cseval = new CfsSubsetEval();
		GreedyStepwise gsearch = new GreedyStepwise();
		
		gsearch.setSearchBackwards(true);
		J48 base = new J48();
		classifier.setClassifier(base);
		classifier.setEvaluator(cseval);
		classifier.setSearch(gsearch);
		
		//评估器	
		Evaluation eval = new Evaluation(data);
		eval.crossValidateModel(classifier, data, 10, new Random(1234));
		System.out.println(eval.toSummaryString());
		
		
		
		
		
		
		
		
	}

}
