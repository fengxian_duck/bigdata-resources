package sdust.lab207.viewable;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

/**
 * @author LbZhang
 * @version 创建时间：2016年6月17日 下午4:39:34
 * @description 可视化决策树
 * 
 */
public class VilsualizeTree {

	public static void main(String[] args) throws Exception {
		Instances data = DataSource.read("data\\weather.nominal.arff");
		// 设置类别属性索引
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes() - 1);
		}
		
		J48 cls = new J48();
		cls.buildClassifier(data);
		
		//显示树
		TreeVisualizer tv = new TreeVisualizer(null, cls.graph(), new  PlaceNode2());
		
		JFrame jf = new JFrame("J48决策树可视化");
		jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jf.setSize(800,600);
		jf.getContentPane().setLayout(new BorderLayout());
		jf.getContentPane().add(tv,BorderLayout.CENTER);
		jf.setVisible(true);
		
		tv.fitToScreen();
		
		
		
		
		
		
	}

}
