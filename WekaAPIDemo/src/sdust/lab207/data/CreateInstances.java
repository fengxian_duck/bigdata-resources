package sdust.lab207.data;

import java.util.ArrayList;
import java.util.Random;

import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

/**
 * @author LbZhang
 * @version 创建时间：2016年6月13日 下午8:09:47
 * @description 内存数据整理 在内存中创建数据集，然后讲述如何打乱数据顺序，使适应学习方案的要求
 *              创建数据集需要通过编程在内存中生成数据集存储结构（即是weka.core.Instances对象），这是一个两阶段的过程：
 *              第一：通过设置属性定义数据格式； 第二：一行一行的添加实际数据。
 * 
 * 
 * 
 */
public class CreateInstances {

	public static void main(String[] args) throws Exception {
		// Instances data = DataSource.read("data\\cpu.arff");
		ArrayList<Attribute> atts;
		ArrayList<Attribute> attsRel;
		ArrayList<String> attVals;
		ArrayList<String> attValsRel;
		Instances data;
		Instances dataRel;
		double[] vals;
		double[] valsRel;
		int i;

		// 1.设置属性
		atts = new ArrayList<Attribute>();
		// 数值型
		atts.add(new Attribute("att1"));
		// 标称型 标称属性是有限集合属性
		attVals = new ArrayList<String>();
		// 需要创建标签
		for (i = 0; i < 5; i++) {
			attVals.add("val" + (i + 1));
		}
		atts.add(new Attribute("att2", attVals));
		// 字符串型
		atts.add(new Attribute("att3", (ArrayList<String>) null));
		// 日期型
		atts.add(new Attribute("att4", "yyyy-MM-dd"));
		// 关系型
		attsRel = new ArrayList<Attribute>();
		// 数值型
		attsRel.add(new Attribute("att5.1"));
		// 标称型
		attValsRel = new ArrayList<String>();
		// 需要创建标签
		for (i = 0; i < 5; i++) {
			attValsRel.add("val5." + (i + 1));
		}
		
		attsRel.add(new Attribute("att5.2",attValsRel));
		dataRel=new Instances("att5",attsRel,0);
		atts.add(new Attribute("att5",dataRel,0));
		
		//2. 创建Instances对象
		data = new Instances("MyRelation",atts,0);
		
		//3. 添加数据
		//第一个实例
		vals = new double[data.numAttributes()];
		vals[0]=Math.PI;//数值
		vals[1]=attVals.indexOf("val3");//标称
		vals[2]=data.attribute(2).addStringValue("A String.");
		vals[3]=data.attribute(3).parseDate("2016-06-14");
		dataRel=new Instances(data.attribute(4).relation(),0);
		///第一个实例
		valsRel=new double[2];
		valsRel[0]=Math.PI+1;
		valsRel[1]=attValsRel.indexOf("val5.3");
		dataRel.add(new DenseInstance(1.0,valsRel));
		///第二个实例
		valsRel=new double[2];
		valsRel[0]=Math.PI+2;
		valsRel[1]=attValsRel.indexOf("val5.2");
		dataRel.add(new DenseInstance(1.0,valsRel));
		
		vals[4]=data.attribute(4).addRelation(dataRel);
		
		data.add(new DenseInstance(1.0,vals));//添加到Instance对象
		
		
		//第二个实例
		vals = new double[data.numAttributes()];
		vals[0]=Math.E;//数值
		vals[1]=attVals.indexOf("val1");//标称
		vals[2]=data.attribute(2).addStringValue("Yet Another String.");
		vals[3]=data.attribute(3).parseDate("2016-06-10");
		dataRel=new Instances(data.attribute(4).relation(),0);
		///第一个实例
		valsRel=new double[2];
		valsRel[0]=Math.E+1;
		valsRel[1]=attValsRel.indexOf("val5.4");
		dataRel.add(new DenseInstance(1.0,valsRel));
		///第二个实例
		valsRel=new double[2];
		valsRel[0]=Math.E+2;
		valsRel[1]=attValsRel.indexOf("val5.1");
		dataRel.add(new DenseInstance(1.0,valsRel));
		
		vals[4]=data.attribute(4).addRelation(dataRel);
		
		data.add(new DenseInstance(1.0,vals));//添加到Instance对象
		
		//4. 输出数据
//		System.out.println(data);
		Instances data1 = new Instances(data);
		data1.randomize(new Random());
		System.out.println("使用默认构造函数后第1次数据集内容：");
		System.out.println(data1);
		
		Instances data2 = new Instances(data);
		data2.randomize(new Random());
		System.out.println("使用默认构造函数后第2次数据集内容：");
		System.out.println(data2);
		
		//为random提供随机数种子的构造函数
		long seed=12341;		
		Instances data3 = new Instances(data);
		data3.randomize(new Random(seed));
		System.out.println("使用提供随机数种子构造函数后第一次数据集内容：");
		System.out.println(data3);
		
		Instances data4 = new Instances(data);
		data4.randomize(new Random(seed));
		System.out.println("使用提供随机数种子构造函数后第一次数据集内容：");
		System.out.println(data4);
	}
	
	

}
