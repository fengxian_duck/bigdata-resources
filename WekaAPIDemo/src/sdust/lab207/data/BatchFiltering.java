package sdust.lab207.data;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.filters.unsupervised.attribute.Standardize;

/**
 * @author LbZhang
 * @version 创建时间：2016年6月14日 下午3:53:33
 * @description 批量过滤 批量过滤分开的训练集和测试集，其中训练集用于初始化过滤器，测试集用于过滤数据。
 *              segment-challenge.arff
 */
public class BatchFiltering {

	public static void main(String[] args) throws Exception {
		// 加载数据集
		Instances train = DataSource.read("data\\segment-challenge.arff");
		Instances test = DataSource.read("data\\segment-test.arff");

		//过滤数据
		//使用标准化过滤器
		Standardize filter = new Standardize();
		//基于训练集一次初始化过滤器
		filter.setInputFormat(train);
		//基于训练集配置过滤器，并且返回过滤后的实例  过滤器配置过滤器
		Instances newTrain = Filter.useFilter(train, filter);
		//过滤并创建新的测试集
		Instances newTest = Filter.useFilter(test, filter);
		
		//输出数据集
		System.out.println("新训练集：");
		System.out.println(newTrain);
		
		System.out.println("----------------------------------------");
		System.out.println("新测试集");
		System.out.println(newTest);
		
		
		
		
	}

}
