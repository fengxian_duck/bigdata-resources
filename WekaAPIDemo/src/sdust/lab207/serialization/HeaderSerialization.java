package sdust.lab207.serialization;

import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.ConverterUtils.DataSource;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月17日 下午9:19:21 
 * @description 头信息序列化
 * 
 */
public class HeaderSerialization {
	
	public static void main(String[] args) throws Exception {
		Instances train = DataSource.read("data\\segment-challenge.arff");
		// 设置类别属性索引
		if (train.classIndex() == -1) {
			train.setClassIndex(train.numAttributes() - 1);
		}
		
		Classifier cls = new J48();
		cls.buildClassifier(train);
		
		//序列化分类器以及头信息
		
		Instances header = new Instances(train,0);
		SerializationHelper.writeAll("data\\jj48.model", new Object[]{cls,header});
		System.out.println("序列化分类器以及头信息成功");
		System.out.println("----------------------------------");

		//加载测试集
		Instances test = DataSource.read("data\\segment-test.arff");
		test.setClassIndex(test.numAttributes()-1);
		
		//反序列化模型
		
		Object[] ob = SerializationHelper.readAll("data\\jj48.model");
		Classifier cls2 = (Classifier) ob[0];
		Instances data = (Instances) ob[1];
		
		//模型和测试集是否兼容
		if(!data.equalHeaders(test)){
			throw new Exception("数据不兼容");
		}
		
		
		System.out.println("反序列化分类器和头信息成功");
		System.out.println("反序列化模型如下：");
		System.out.println(cls2);
		
		
	}
	
	
	

}
