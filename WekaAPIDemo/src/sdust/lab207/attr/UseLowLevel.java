package sdust.lab207.attr;

import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月17日 下午3:00:25 
 * @description 使用底层API 
 * 使用底层API进行属性选择
 */
public class UseLowLevel {
	
	public static void main(String[] args) throws Exception {
		Instances data = DataSource.read("data\\weather.numeric.arff");

		// 设置类别属性索引
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes() - 1);
		}
		
		System.out.println("使用底层API ");
		AttributeSelection attsel = new AttributeSelection();
		CfsSubsetEval cseval = new CfsSubsetEval();
		GreedyStepwise search = new GreedyStepwise();
		search.setSearchBackwards(true);
		
		attsel.setEvaluator(cseval);
		attsel.setSearch(search);
		attsel.SelectAttributes(data);
		int[] indices = attsel.selectedAttributes();
		System.out.println("选择属性索引：(从0开始)");
		Utils.arrayToString(indices);
		for(int i=0;i<indices.length;i++){
			System.out.println(indices[i]);
		}
		System.out.println("属性索引的长度："+indices.length);
		
		
		
		
		
		
	}

}
