package sdust.lab207.attr;

import java.io.File;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
/** 
 * @author LbZhang
 * @version 创建时间：2016年6月17日 下午3:49:19 
 * @description 类说明
 */


public class SimpleAttributeSelection {
 
    /**
     * @param args
     */
    public static void main(String[] args) {
       // TODO Auto-generated method stub
       Instances trainIns = null;//训练实例对象集合
      
       try{
          
           /*
            * 1.读入训练
            * 在此我们将训练样本和测试样本是由weka提供的segment数据集构成的
            */
           File file= new File("data//segment-challenge.arff");
           ArffLoader loader = new ArffLoader();
           loader.setFile(file);
           trainIns = loader.getDataSet();
          
           //在使用样本之前一定要首先设置instances的classIndex，否则在使用instances对象是会抛出异常
           trainIns.setClassIndex(trainIns.numAttributes()-1);
          
           /*
            * 2.初始化搜索算法（search method）及属性评测算法（attribute evaluator）
            */
           Ranker rank = new Ranker();
           InfoGainAttributeEval eval = new InfoGainAttributeEval();//信息增益的评测算法
          
           /*
            * 3.根据评测算法评测各个属性
            */
           eval.buildEvaluator(trainIns);
           //System.out.println(rank.search(eval, trainIns));
          
           /*
            * 4.按照特定搜索算法对属性进行筛选
            * 在这里使用的Ranker算法仅仅是属性按照InfoGain的大小进行排序
            */
           int[] attrIndex = rank.search(eval, trainIns);
          
           /*
            * 5.打印结果信息
            * 在这里我们了属性的排序结果同时将每个属性的InfoGain信息打印出来
            */
           StringBuffer attrIndexInfo = new StringBuffer();
           StringBuffer attrInfoGainInfo = new StringBuffer();
           attrIndexInfo.append("Selected attributes:");
           attrInfoGainInfo.append("Ranked attributes:\n");
           for(int i = 0; i < attrIndex.length; i ++){
              attrIndexInfo.append(attrIndex[i]);
              attrIndexInfo.append(",");
             
              attrInfoGainInfo.append(eval.evaluateAttribute(attrIndex[i]));
              attrInfoGainInfo.append("\t");
              attrInfoGainInfo.append((trainIns.attribute(attrIndex[i]).name()));
              attrInfoGainInfo.append("\n");
           }
           System.out.println(attrIndexInfo.toString());
           System.out.println(attrInfoGainInfo.toString());
          
       }catch(Exception e){
           e.printStackTrace();
       }
    }
 
}
