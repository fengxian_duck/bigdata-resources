package sdust.lab207.data;

import java.util.Random;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月14日 下午2:56:27 
 * @description 简单的过滤器使用
 */
public class AddFilter {
	
	public static void main(String[] args) throws Exception {
		//加载数据集
		Instances data = DataSource.read("data\\weather.numeric.arff");
		Instances result = null;
		
		Add filter;
		result = new Instances(data);
		
		//新增数值属性
		filter = new Add();
		filter.setAttributeIndex("last");
		filter.setAttributeName("NumericAttribute");
		filter.setInputFormat(result);
		
		result = Filter.useFilter(result, filter);
		
		//新增标称属性
		filter = new Add();
		filter.setAttributeIndex("last");
		filter.setNominalLabels("A,B,C");;
		filter.setInputFormat(result);		
		result = Filter.useFilter(result, filter);
		
		//用随机数填充新增的两个属性
		Random rand = new Random(1234);
		for(int i=0;i<result.numInstances();i++){
			//填充数值属性
			result.instance(i).setValue(result.numAttributes()-2, rand.nextDouble());
			//填充标称属性
			result.instance(i).setValue(result.numAttributes()-1, rand.nextInt(3));
			
		}
		
		System.out.println("过滤后的数据集：");
		System.out.println(result);
		
	}

}
