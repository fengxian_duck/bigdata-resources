/**
 * 序列化学习
 * 
 */
/**
 * @author MrLBZ
 *序列化是将一个对象保存为一种保存为一种持久形式（如在硬盘上的一个字节流）的过程。反序列化是相反的过程，是从持久化的数据结构中创建一个对象。
 *在Java中，一个对象如果是想了java.io.Serializable接口则可以序列化。如果序列化对象的某个成员用不着序列化，则需要transient关键字声明。
 *
 */
package sdust.lab207.serialization;