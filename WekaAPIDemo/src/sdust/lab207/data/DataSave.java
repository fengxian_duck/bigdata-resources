package sdust.lab207.data;

import java.io.File;

import weka.core.Instances;
import weka.core.converters.CSVSaver;
import weka.core.converters.DatabaseSaver;
import weka.core.converters.ConverterUtils.DataSink;
import weka.core.converters.ConverterUtils.DataSource;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月13日 下午4:30:41 
 * @description 保存数据
 */
public class DataSave {
	
	public static void main(String[] args) throws Exception {
		//DataSave.saveByFile();
		DataSave.saveByDataBase();
	}

	/**
	 * 首先从文件加载数据然后批量的方式或者增量的方式将数据保存到数据库中。
	 * @throws Exception 
	 */
	private static void saveByDataBase() throws Exception {
		
		Instances data = DataSource.read("data\\cpu.arff");
		System.out.println("加载数据完成……");
		
		DatabaseSaver dsaver = new DatabaseSaver();
		//批量保存
		dsaver.setDestination("jdbc:mysql://127.0.0.1:3306/jdtaobao","root","root");
		//明确表名称
		dsaver.setTableName("ircpu");
		dsaver.setRelationForTableName(false);
		dsaver.setInstances(data);
		dsaver.writeBatch();
		
		//增量方式保存
		DatabaseSaver dsaver2 = new DatabaseSaver();
		//批量保存
		dsaver2.setDestination("jdbc:mysql://127.0.0.1:3306/jdtaobao","root","root");
		dsaver2.setTableName("ircpu");
		dsaver2.setRelationForTableName(false);
		dsaver2.setRetrieval(DatabaseSaver.INCREMENTAL);
		dsaver2.setStructure(data);
//		dsaver.setInstances(data);
		for(int i=0;i<data.numInstances();i++){
			dsaver2.writeIncremental(data.get(i));
		}
		dsaver2.writeIncremental(null);
		System.out.println("完成增量方式的保存数据");
		
	}

	private static void saveByFile() throws Exception {
		Instances data = DataSource.read("data\\cpu.arff");
		//使用DataSink类
		DataSink.write("save\\ccpu.arff", data);
		DataSink.write("save\\ccpu.csv", data);
		
		//明确指定转换器
		CSVSaver saver = new CSVSaver();
		saver.setInstances(data);
		
		saver.setFile(new File("save\\ccsv.csv"));
		saver.writeBatch();
		
	}

}
