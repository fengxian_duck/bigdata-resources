package sdust.lab207.viewable;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import weka.classifiers.bayes.BayesNet;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.gui.beans.GraphViewer;
import weka.gui.graphvisualizer.GraphVisualizer;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

/**
 * @author LbZhang
 * @version 创建时间：2016年6月17日 下午4:39:34
 * @description 可视化贝叶斯网络
 * 
 */
public class VisualizeBayesNet {

	public static void main(String[] args) throws Exception {
		Instances data = DataSource.read("data\\weather.nominal.arff");
		// 设置类别属性索引
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes() - 1);
		}
		
		BayesNet cls = new BayesNet();
		cls.buildClassifier(data);
		
		//显示图形
		//图的可视化器
		GraphVisualizer gv = new GraphVisualizer();
		gv.readBIF(cls.graph());
		
		JFrame jf = new JFrame("BayesNet可视化");
		jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jf.setSize(800,600);
		jf.getContentPane().setLayout(new BorderLayout());
		jf.getContentPane().add(gv,BorderLayout.CENTER);
		jf.setVisible(true);
		
		gv.layoutGraph();
		
		
		
		
		
		
	}

}
