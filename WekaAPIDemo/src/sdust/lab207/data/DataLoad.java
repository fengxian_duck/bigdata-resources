package sdust.lab207.data;

import java.io.File;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.DatabaseLoader;
import weka.core.converters.ConverterUtils.DataSource;
import weka.experiment.InstanceQuery;

/**
 * @author LbZhang
 * @version 创建时间：2016年6月10日 上午10:16:23
 * @description 数据加载 weka.core.Instances;
1.Weka处理的数据表格中，一个横行称为一个实例（Instance）,竖行代表一个属性（Arrtibute），数据表格称为一个数据集，在weka看来，呈现了属性之间的一种关系（Relation）
2.Weka存储数据的格式是ARFF（Attribute-RelationFile Format）文件，这是一种ASCII文本文件。
3.Weka的ARFF文件可以分为两部分。第一部分给出了头信息（Head information）,包括了对关系的声明和对属性的声明。第二部分给出了数据信息（Data information），即数据集中给出的数据。从@Data标记开始，后面的就是数据信息了。
4.Weka作为数据挖掘，面临的第一个问题往往是我们的数据不是ARFF格式的。幸好，WEKA还提供了对CSV文件的支持，而这种格式是被许多其他软件所支持的。此外，WEKA还提供了通过JDBC访问数据库的功能。 
 */
public class DataLoad {

	/**
	 * Open Declaration weka.core.converters.ConverterUtils.DataSource
	 * DataSource(数据源)类是weka.core.converters.ConverterUtils的内部类，用于从有适当文件扩展名的文件中读取数据。
	 * 
	 * 
	 */

	public static void main(String[] args) {
		
		//DataLoad.testFileload();
		DataLoad.testDBload();
		
	}

	private static void testDBload() {
		try {
			//InstanceQuery使用
			InstanceQuery iq = new InstanceQuery();
			iq.setDatabaseURL("jdbc:mysql://127.0.0.1:3306/jdtaobao");
			iq.setUsername("root");
			iq.setPassword("root");
			iq.setQuery("SELECT * FROM tb_timestat");
			//iq.setSparseData(true);
			Instances ist = iq.retrieveInstances();
			
			System.out.println(ist.checkForStringAttributes());
			
			System.out.println(ist.get(0));
			System.out.println(ist.attributeStats(0));
		   // System.out.println(ist.get(2));
			
			DatabaseLoader dloader = new DatabaseLoader();
			String jdurl="jdbc:mysql://127.0.0.1:3306/jdtaobao";
			String user = "root";
			String pass = "root";
			dloader.setSource(jdurl,user,pass);
			dloader.setQuery("SELECT * FROM tb_timestat");
			//批量检索
			Instances data = dloader.getDataSet();
//			System.out.println(data.classIndex());
//			System.out.println(data.size());
//			System.out.println(data.get(0));
			System.out.println(data.get(data.size()-1));
			System.out.println(data);
			System.out.println();
			
			//增量检索
			DatabaseLoader diloader = new DatabaseLoader();
			diloader.setSource(jdurl,user,pass);
			diloader.setQuery("SELECT * FROM tb_user");
			
			Instances structure = diloader.getStructure();
			
			Instances insts = new Instances(structure);
			Instance inst ;
			while((inst=diloader.getNextInstance(structure))!=null){
				System.out.println(inst);
				insts.add(inst);
			}
			System.out.println(insts);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static void testFileload() {
		try {

			// 读取数据代码片段
			Instances data1 = DataSource.read("data\\cpu.arff");
			Instances data2 = DataSource.read("data\\cpu.arff");
			// 当要加载的文件的与加载器通常关联的文件扩展名不同时，用户只能直接指定加载器。
			// 加载arrf文件代码片段
			ArffLoader loader = new ArffLoader();
			loader.setSource(new File("data\\cpu.arff"));
			Instances data = loader.getDataSet();// 获取数据集合

			System.out.println(data.classIndex());

			// 如果没有设置类别属性
			if (data.classIndex() == -1)
				data.setClassIndex(0);
			// 使用第一个属性作为类别属性			
			if (data.classIndex() == -1)
				data.setClassIndex(data.numAttributes()-1);

			if (data.classIndex() == -1) {//如果没有设置类别属性列
				System.out.println(data1.get(0));
			}
			//System.out.println(data.attribute(0));
			
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
