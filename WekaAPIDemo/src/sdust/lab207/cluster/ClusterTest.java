package sdust.lab207.cluster;

import java.io.File;

import weka.clusterers.ClusterEvaluation;
import weka.clusterers.Cobweb;
import weka.clusterers.DensityBasedClusterer;
import weka.clusterers.EM;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ArffLoader;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月15日 下午10:30:08 
 * @description Weka聚类的Java调用实现
 * ==================================
 */
public class ClusterTest {

	/**
	 * 主函数入口
	 * @param args
	 * @throws Exception 未对异常进行处理
	 */
	public static void main(String[] args) throws Exception {
		//ClusterTest.testBatchCluster();
		//ClusterTest.testIncrementalCluster();
		//ClusterTest.ClusterEvaluation();
		//ClusterTest.classToClusters();
		ClusterTest.outputClusterDistribution();
		
	}

	/**
	 * 输出聚类分布
	 * @throws Exception 
	 */
	private static void outputClusterDistribution() throws Exception {
		Instances train = DataSource.read("data\\segment-challenge.arff");
		Instances test = DataSource.read("data\\segment-test.arff");
		
		if(!train.equalHeaders(test)){
			throw new Exception("训练集和测试集不兼容"+train.equalHeadersMsg(test));
		}
		
		//构建聚类器
		EM em = new EM();
		em.buildClusterer(train);
		
		//输出预测
		for(int i=0;i<test.numInstances();i++){
			int clu = em.clusterInstance(test.instance(i));
			//隶属度的计算distributionForInstance
			double[] dist = em.distributionForInstance(test.instance(i));
			
			System.out.print(i+1);
			System.out.print(" - ");
			System.out.print(clu);
			System.out.print(" - "+dist.length +"  === ");
			//System.out.print(" - ");
			System.out.print(Utils.arrayToString(dist));
			System.out.println();
			
		}
	}

	/*
	 * 评估主要是用于比较选择的簇和预定类别的匹配程度*/
	private static void classToClusters() throws Exception {
		ArffLoader loader = new ArffLoader();
		loader.setFile(new File("data\\contact-lenses.arff"));
		Instances data = loader.getDataSet();
		
		data.setClassIndex(data.numAttributes()-1);
		
		//生成聚类器数据 过滤去除的类别属性
		Remove filter = new Remove();
		filter.setAttributeIndices(""+(data.classIndex()+1));
		filter.setInputFormat(data);
		Instances dataClusterer = Filter.useFilter(data, filter);
		
		//训练聚类器
		EM em = new EM();
		em.buildClusterer(dataClusterer);
		
		
		//评估聚类器
		ClusterEvaluation ceval = new ClusterEvaluation();
		ceval.setClusterer(em);
		ceval.evaluateClusterer(data);
		
		//输出结果
		System.out.println(ceval.clusterResultsToString());
		
		
		
		
	}

	/**
	 * 三种评估聚类器的方式
	 * @throws Exception 
	 * 
	 */
	private static void ClusterEvaluation() throws Exception {
		
		String  filename = "data\\contact-lenses.arff";
		ClusterEvaluation ceval;
		
		Instances data;
		String[] options;
		DensityBasedClusterer dbc;
		double logLikelyhood;
		
		//加载数据
		data=DataSource.read(filename);
		
		//常规方法  直接使用-t选择训练文件，然后调用ClusterEvaluation的静态方法evaluateClusterer  默认执行十折交叉验证
		System.out.println("Common mothods:");
		options=new String[2];
		options[0]="-t";
		options[1]=filename;
		
		String output = ClusterEvaluation.evaluateClusterer(new EM(), options);
		System.out.println(output);
		
		//手工调用  这里是实例化ClusterEvaluation对象，调用该对象的setCluster方法和evaluateClusterer方法。
		System.out.println("Manual mothods:");
		dbc = new EM();
		dbc.buildClusterer(data);
		
		ceval = new ClusterEvaluation();
		ceval.setClusterer(dbc);
		ceval.evaluateClusterer(new Instances(data));
		
		System.out.println(ceval.clusterResultsToString());
		
		
		/**
		 * 基于密度的聚类器交叉验证
		 * 
		 * 直接调用ClusterEvaluation的静态方法crossValidateModel 实现交叉验证
		 * clusterer the clusterer to use
		 * data the training data
		 * numFolds number of folds of cross validation to perform
		 * random random number seed for cross-validation

		 */
		System.out.println("基于密度的聚类器交叉验证");
		dbc = new EM();
		logLikelyhood = ClusterEvaluation.crossValidateModel(dbc, data, 10,data.getRandomNumberGenerator(1234));
		System.out.println("对数似然"+logLikelyhood );
		
		
		
		
		
	}

	/**
	 * 增量聚类 增量聚类聚类器必须实现wekaclusters包的updateableCluster接口。
	 * 目前weka实现了updateableCluster接口的聚类算法只有Cobweb
	 * @throws Exception 
	 */
	private static void testIncrementalCluster() throws Exception {
		//加载数据
		ArffLoader loader = new ArffLoader();
		loader.setFile(new File("data\\contact-lenses.arff"));
		Instances structure = loader.getStructure();

		//训练cobweb
		Cobweb cw = new Cobweb();
		cw.buildClusterer(structure);
		Instance current;
		
		while((current=loader.getNextInstance(structure))!=null){
			cw.updateClusterer(current);
			
		}
		
		cw.updateFinished();
		System.out.println(cw);
		
		
		
	}

	/**
	 * 批量聚类
	 * @throws Exception
	 */
	private static void testBatchCluster() throws Exception {
		ArffLoader loader = new ArffLoader();
		loader.setFile(new File("data\\contact-lenses.arff"));
		Instances data = loader.getDataSet();
		
		//构建聚类器
		String[] op = new String[2];
		op[0]="-I";//最大迭代次数
		op[1]="100";
		
		EM emc = new EM();
		emc.setOptions(op);//传入参数数组
		emc.buildClusterer(data);//生成聚类器
		
		System.out.println(emc);
		
	}
}
