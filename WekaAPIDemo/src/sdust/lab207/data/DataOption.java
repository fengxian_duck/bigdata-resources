package sdust.lab207.data;

import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;


/** 
 * @author LbZhang
 * @version 创建时间：2016年6月13日 下午5:30:34 
 * @description 处理选项
 */
public class DataOption {
	
	public static void main(String[] args) throws Exception {
		
		Instances data = DataSource.read("data\\cpu.arff");
		
		System.out.println("Show the content of DateSet");
		System.out.println(data);
		
		//Assemble String Arrays 不by handcraft
		System.out.println("Assemble String Arrays 不by handcraft:");
		String[] options = new String[2];
		//删除第一个属性列
		options[0]="-R";
		options[1]="1";
		
		Remove rm = new Remove();
		rm.setOptions(options);
		//数据集过滤
		rm.setInputFormat(data);
		Instances inst1 = Filter.useFilter(data, rm);
		System.out.println("数据集过滤后的内容:");
		System.out.println(inst1);
		
		
		
		//使用weka.core.Utils类的splitOptions(String)方法
		System.out.println("使用weka.core.Utils类的splitOptions(String)方法");
		String[] options2 = Utils.splitOptions("-R 1");
		Remove rm2 = new Remove();
		rm2.setOptions(options);
		//数据集过滤
		rm2.setInputFormat(data);
		Instances inst2 = Filter.useFilter(data, rm2);
		System.out.println("数据集过滤后的内容inst2:");
		System.out.println(inst2);
		
		
		//使用属性的set方法
		System.out.println("使用属性的set方法");
		Remove rm3 = new Remove();
		rm3.setAttributeIndices("1");
		//数据集过滤
		rm3.setInputFormat(data);
		Instances inst3 = Filter.useFilter(data, rm3);
		System.out.println("数据集过滤后的内容inst3:");
		System.out.println(inst3);
		
		
		
		
		
		
		
	}

}
