package sdust.lab207.data;

import java.io.File;
import java.io.IOException;

import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月15日 下午12:27:35 
 * @description 批量分类器构建
 * 
 */
public class BatchClassifier {
	
	/**
	 * 批量分类器
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		ArffLoader loader = new ArffLoader();
		loader.setFile(new File("data\\weather.nominal.arff"));
		Instances inst = loader.getDataSet();
		inst.setClassIndex(inst.numAttributes()-1);//最后一列最为分类属性
		
		//训练J48分类器
		String[] options = new String[1];
		options[0]="-U";
		J48 tree = new J48();
		
		tree.setOptions(options);
		tree.buildClassifier(inst);
		
		//输出模型
		
		System.out.println(tree);
		
		
		
		
		
		
	}

}
