package sdust.lab207.viewable;

import java.awt.BorderLayout;
import java.util.Random;

import javax.swing.JFrame;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.evaluation.ThresholdCurve;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;
import weka.gui.visualize.PlotData2D;
import weka.gui.visualize.ThresholdVisualizePanel;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月17日 下午8:24:24 
 * @description 可视化ROC曲线
 */
public class VisualizeROC {
	
	public static void main(String[] args) throws Exception {
		Instances data = DataSource.read("data\\weather.nominal.arff");
		// 设置类别属性索引
		if (data.classIndex() == -1) {
			data.setClassIndex(data.numAttributes() - 1);
		}
		
		//评估分类器
		Classifier classifier = new NaiveBayes();
		Evaluation eval = new Evaluation(data);//评估器
		eval.crossValidateModel(classifier, data, 10, new Random(1234));
		
		//第一步生成可以绘制的数据
		ThresholdCurve tc = new ThresholdCurve();
		int classIndex = 0;
		Instances curve = tc.getCurve(eval.predictions(),classIndex);
		
		//第二步将可以绘制的数据放入到绘图容器
		
		PlotData2D plotdata = new PlotData2D(curve);
		plotdata.setPlotName(curve.relationName());
		
		plotdata.addInstanceNumberAttribute();
		
		
		//将绘图容器添加到可视化面板
		ThresholdVisualizePanel tvp = new ThresholdVisualizePanel();
		tvp.setROCString("(Area under ROC = "+Utils.doubleToString(ThresholdCurve.getROCArea(curve), 4)+")");
		
		tvp.setName(curve.relationName());
		
		//指定链接那些点
		boolean[] cp = new boolean[curve.numInstances()];
		for(int i=1;i<cp.length;i++){
			cp[i]=true;
		}
		
		plotdata.setConnectPoints(cp);
		tvp.addPlot(plotdata);
		
		
		//第四步 将可视化面板添加到JFrame
		JFrame jf = new JFrame("WEKA ROC : "+tvp.getName());
		//jf.setSize(500,400);
		
		jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		jf.setSize(800,600);
		jf.getContentPane().setLayout(new BorderLayout());
		jf.getContentPane().add(tvp,BorderLayout.CENTER);
		jf.setVisible(true);
		
		

		
		
		
		
		
		
		
		
		
		
	}

}
