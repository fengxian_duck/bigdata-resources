package sdust.lab207.data;

import java.util.Random;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSink;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AddClassification;



/** 
 * @author LbZhang
 * @version 创建时间：2016年6月15日 下午6:41:55 
 * @description 单次运行交叉验证
 * 
 */
public class RunOnceCV {
	public static void main(String[] args) throws Exception {
		//加载数据
		Instances data = DataSource.read("data\\ionosphere.arff");
		//设置类别索引
		data.setClassIndex(data.numAttributes()-1);
		
		//分类器
		String[] options = new String[2];
		String classname = "weka.classifiers.trees.J48";
		
		options[0]="-C";
		options[1]="0.25";
		//分类器的声明定义
		Classifier classifier = (Classifier)Utils.forName(Classifier.class, classname, options);
		
		//其他选项
		int seed = 1234;
		int folds = 10;
		
		Random rand = new Random(seed);
		
		Instances newData = new Instances(data);
		newData.randomize(rand);
		
		//如果类别为标称型则根据类别之进行分层
		
		if(newData.classAttribute().isNominal()){
			newData.stratify(folds);
		}
		
		Instances predictedData = null; 
		//执行交叉验证
		
		Evaluation eval = new Evaluation(newData);
		
		for(int i=0;i<folds;i++){
			//训练集
			Instances train = newData.testCV(folds, i);
			//测试集
			Instances test = newData.testCV(folds, i);
			
			//构建并且评估分类器
			Classifier clsCopy = AbstractClassifier.makeCopy(classifier);
			clsCopy.buildClassifier(train);
			eval.evaluateModel(clsCopy, test);
			
			//交叉验证并且 预测
			AddClassification filter = new AddClassification();
			filter.setClassifier(classifier);//设置分类器
			filter.setOutputClassification(true);
			filter.setOutputDistribution(true);
			filter.setOutputErrorFlag(true);
			filter.setInputFormat(train);//添加训练集
			//训练分类器
			Filter.useFilter(test, filter);
			//在测试集上预测
			Instances pred = Filter.useFilter(test, filter);
			
			if(predictedData==null){
				predictedData=new Instances(pred,0);//防止预测数据集合为空
			}
			
			for(int j=0;j<pred.numInstances();j++){
				predictedData.add(pred.instance(j));//添加到预测数据集上
			}
			
			
			
			
			
		}
		
		System.out.println();
		System.out.println("=====分类器设置=====");
		if(classifier instanceof OptionHandler){
			System.out.println("分类器是否实现oph接口："+classifier.getClass().getName()+Utils.joinOptions(((OptionHandler)classifier).getOptions()));
		}else{
			System.out.println("分类器："+classifier.getClass().getName() );
		}
		
		//System.out.println("分类器"+Utils.toCommandLine(classifier));
		System.out.println("数据集"+data.relationName());
		System.out.println("折数"+folds);
		System.out.println();
		
		System.out.println(eval.toSummaryString("====="+folds+"折交叉验证===",false));
		
		DataSink.write("data\\prediction.arff", predictedData);
		
		
		
		
	}

}
