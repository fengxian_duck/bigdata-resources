package sdust.lab207.data;

import java.io.File;

import weka.classifiers.bayes.NaiveBayesUpdateable;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

/** 
 * @author LbZhang
 * @version 创建时间：2016年6月15日 下午3:30:27 
 * @description 增量分类器的构建
 */
public class IncrementalClassifier {
	
	public static void main(String[] args) throws Exception {
		
		/**
		 * 记载数据
		 */
		ArffLoader loader = new ArffLoader();
		loader.setFile(new File("data\\weather.nominal.arff"));
		Instances structure = loader.getStructure();		
		structure.setClassIndex(structure.numAttributes()-1);//最后一列最为分类属性
		
		NaiveBayesUpdateable nb = new NaiveBayesUpdateable();
		
		nb.buildClassifier(structure);
		Instance inst;
		while((inst=loader.getNextInstance(structure))!=null){
			nb.updateClassifier(inst);
		}
		
		System.out.println(nb);
		
		
	}

}
